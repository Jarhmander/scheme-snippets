(import (rnrs io ports)
        (rnrs bytevectors)
        (ice-9 iconv))

(define (with-output-to-bytevector proc)
  (call-with-values
    open-bytevector-output-port
    (lambda (port bv-get)
      (proc port)
      (let ((ret (bv-get)))
        (close-port port)
        ret))))

(define uint->bytevector 
  (case-lambda
    ((len value endian)
     (let ((ret (make-bytevector len)))
       (bytevector-uint-set! ret 0 value endian len)
       ret))
    ((len value)
     (uint->bytevector len value (endianness little)))))
