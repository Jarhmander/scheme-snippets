(import (srfi :1)
        (rnrs bytevectors))

(define (bv-cat . bvs)
  (define ret
    (let ((len (fold (lambda (e a)
                       (+ a (bytevector-length e)))
                     0 bvs)))
      (make-bytevector ret-len)))
  (let cat ([i 0] [bvs bvs])
    (if (null? bvs)
        ret
        (let* ([bv     (car bvs)]
               [bv-len (bytevector-length bv)])
          (bytevector-copy! bv 0 ret i bv-len)
          (cat (+ i bv-len) (cdr bvs))))))
