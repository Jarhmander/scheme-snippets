#!/usr/bin/guile -s
!#

(import (ice-9 popen)
        (ice-9 threads)
        (ice-9 format)
        (rnrs io ports)
        (srfi :1)
        (srfi :69))

(define (read-process-to-list cmd . args)
  (call-with-port (open-input-pipe (apply format #f cmd args))
    (lambda (p)
      (let lp ()
        (define e (read p))
        (if (eof-object? e)
            '()
            (cons e (lp)))))))

(define package-size (read-process-to-list "expac '(%n . %m)'"))
(define package-deps (read-process-to-list "expac '(%n %E)'"))

(define package-deps-size (make-hash-table))

(define package-provides-map
  (let ()
    (define h (make-hash-table))
    (define p (filter (lambda (e) (> (length e) 1))
                      (read-process-to-list "expac '(%n %S)'")))

    (for-each
      (lambda (pkg)
        (let ((pkg-name (car pkg)))
          (for-each
            (lambda (provide)
              (hash-table-set! h provide pkg-name))
            (cdr pkg))))
      p)
    h))

(for-each
  (lambda (pkg-deps)
    (define (get pkg)
      (let ((pkg (hash-table-ref/default package-provides-map pkg pkg)))
        (define ret (hash-table-ref/default package-deps-size pkg #f))
        (define pkg-size (assq-ref package-size pkg))
        (cond
          ((not pkg-size)
           (error "package not found (in package-size): " pkg))
          ((not ret)
           ;; For detecting cycles, see below
           (hash-table-set! package-deps-size pkg pkg)
           (let ((deps (assq-ref package-deps pkg)))
             (if deps
                 (let* ((m (map get deps))
                        (s (apply + pkg-size m)))
                   (hash-table-set! package-deps-size pkg s)
                   s)
                 (error "package not found for deps: " pkg))))
          ((symbol? ret)
           (format (current-error-port)
                   "warning: dependency cycle detected for package ~A with ~A~%" ret pkg)
           pkg-size)
          (else ret))))

    (get (car pkg-deps)))
  package-deps)

(define format-size
  (let ((suffixes '#(B kB MB GB TB)))
    (lambda (size)
      (let* ((power  (if (zero? size)
                         0
                         (inexact->exact (floor (/ (log10 size)
                                                   3)))))
             (suffix (vector-ref suffixes power))
             (scaled (/ size (expt 10 (* power 3)))))
        (format #f "~3,2f~A" scaled suffix)))))

(define max-name-size
  (let ((maxv 0))
    (hash-table-walk package-deps-size
                     (lambda (k _)
                       (set! maxv (max maxv (string-length (symbol->string k))))))
    maxv))

(hash-table-walk package-deps-size
  (let ((col-pos (list (+ max-name-size 8) (+ max-name-size 8 16))))
    (lambda (k v)
      (format #t "~A:~:{~VT~A~}~%" k (zip col-pos
                                          (map format-size
                                               (list
                                                 (assq-ref package-size k)
                                                 v)))))))
