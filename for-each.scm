;;; for-each

(define-syntax for
  (syntax-rules (in do)
    [(for var in exp do
        e1 e* ...)
     (for-each (lambda (var)
                 e1 e* ...)
               exp)]))
