(define-syntax datum
  (syntax-rules ()
    [(_ id) (syntax->datum #'id)]))

(define-syntax datum-list->syntax
  (syntax-rules ()
    [(_ pid l)
     (map (lambda (e) (datum->syntax pid e)) l)]))

(define-syntax with-implicit
  (syntax-rules ()
    [(with-implicit (k pat pat* ...) body body* ...)
     (with-implicit "pat" ((k pat) (k pat*) ...) body body* ...)]

    [(with-implicit "pat" ((k (pat exp)) rest ...) body ...)
     (with-syntax ([pat (datum->syntax
                          #'k
                          (let ([p exp])
                            (if (string? p)
                                (string->symbol p)
                                p)))])
       (with-implicit "pat" (rest ...) body ...))]

    [(with-implicit "pat" ((k var) rest ...) body ...)
     (with-implicit "pat" ((k (var 'var)) rest ...) body ...)]

    [(with-implicit "pat" () body ...)
     (begin body ...)]))
