(use-modules (ice-9 control))

(define (one/each default lst)
  (define cont
    (lambda ()
      (for-each (lambda (e)
                  (shift k
                    (set! cont k)
                    e))
                lst)
      default))
  (lambda ()
    (reset (cont))))
