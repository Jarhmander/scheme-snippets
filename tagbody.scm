
(define-syntax go
  (lambda (stx)
    (lambda (l)
      (syntax-case stx ()
        [(go where)
         (let ([dest (l #'where #'tagbody-continuation)])
           (if dest
               #`(#,dest where)
               (syntax-violation 'go "unknown label" #'where)))]))))

(define-syntax tagbody
  (lambda (stx)
    (define (syntax-list? stx)
      (syntax-case stx ()
        [(e ...) #t]
        [else    #f]))
    (define (make-local-functions stx)
      (let lp ([l    (syntax->list stx)]
               [id   #'start]
               [code '()])
        (define (make-code-segment next)
          (with-syntax ([id   id]
                        [(code ...) (reverse! code)]
                        [next next])
            #'(id (lambda () code ... (next)))))
        (if (null? l)
            #`(#,(make-code-segment #'end)
               (end (lambda () #f)))
            (let-values ([(head tail) (values (car l) (cdr l))])
              (cond
                [(syntax-list? head)
                 (lp tail id (list* head code))]
                [(identifier? head)
                 (cons (make-code-segment head)
                       (lp tail head '()))]
                [else
                  (syntax-error #'(head . tail) "invalid id '" head "' in")])))))
    (syntax-case stx ()
      [(tagbody code ...) (for-all syntax-list? #'(code ...))
       #'(begin code ... (void))]
      [(tagbody id/code ...)
       (with-syntax ([((id fn) ...)  (make-local-functions #'(id/code ...))])
         #'(let ([cont-k #f])
             (define id) ...
             (define-property id tagbody-continuation #'cont-k) ...
             (set! id fn) ...
             (let lp ([k start])
               (let ((k (call/cc
                          (lambda (cont)
                            (set! cont-k cont)
                            (k)))))
                 (if k (lp k))))))])))
