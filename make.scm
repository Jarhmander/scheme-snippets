(import (system foreign))

(define d (dynamic-link))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define format
  (let ([ice-9:format
          (@ (ice-9 format) format)])
    (case-lambda
      [(str) (ice-9:format #f str)]
      [(bool/str . args)
       (if (boolean? bool/str)
           (apply ice-9:format bool/str args)
           (apply ice-9:format #f bool/str args))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (printf . args)
  (apply format #t args))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(let ((fun "gmk_add_function"))
 (printf "dynlink: ~A=~A~%" fun (dynamic-pointer fun d)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#f
