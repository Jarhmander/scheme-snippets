
; First, get access to symbols of shared object already linked (libc).
; We want to get `mmap` and `memcpy` out of libc.
(load-shared-object #f)

; We need `mmap`, to get a page of memory with proper permissions.
(define mmap
  (foreign-procedure "mmap"
    (void* size_t int int int ssize_t) void*))

; Get a mapping for a page with all permissions set.
; The various flags are macros, which would need to be defined here. For simplicity, we
; don't define them, and just use the final values required.
; 7 => rwx
; #x22 => MAP_ANONYMOUS|MAP_PRIVATE
(define code-location (mmap 0 4096 7 #x22 -1 0))

; We have the page, but we have to write to it.
; Let's make an utility that will allow to write a bytevector at some address.
(define foreign-write-bytevector
  (let ([memcpy (foreign-procedure "memcpy"
                  (void* u8* size_t) void*)])
    (lambda (addr bv)
      (memcpy addr
              bv
              (bytevector-length bv)))))

; This is the native-code to run.
; The source code was:
;
;     .text
; code:
;     inc %rdi
;     mov %rdi, %rax
;     ret
;
; The effect, with the x86-64 SysV ABI, is to return its argument incremented by one.
(define code #vu8(#x48 #xFF #xC7 #x48 #x89 #xF8 #xC3))

; Move that code into the code location we previously created.
(foreign-write-bytevector code-location code)

; Now, get an handle to that code.
(define increment
  (foreign-procedure code-location
    (int) int))

; Assert that it works
(assert (= 42 (increment 41)))
