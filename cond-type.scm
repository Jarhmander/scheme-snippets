;;; cond-type

(define (type-of v)
  (define-syntax cond-type
    (lambda (stx)
      (syntax-case stx ()
        [(cond-type types ...)
         (let* ((sym-list (syntax->datum #'(types ...)))
                (clauses (map (lambda (e)
                                 (with-syntax ((v (datum->syntax #'cond-type 'v))
                                               (type? (datum->syntax #'cond-type
                                                                     (symbol-append e '?)))
                                               (ret (datum->syntax #'cond-type
                                                                   (symbol->string e))))
                                   #'((type? v) ret)))
                               sym-list)))
           #`(cond #,@clauses (else #f)))])))
  (cond-type
    boolean number symbol list vector string char procedure bytevector))
