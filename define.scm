
(define tlambda-scan-body
  (lambda (stx)
    (define def-list
      (let* ((lst  (list #f))
             (tail lst))
        (case-lambda
          (()  (cdr lst))
          ((e) (let ((np (cons e '())))
                 (set-cdr! tail np)
                 (set! tail np)
                 np)))))

    (let ((xformed
            (let xform ((stx stx))
              (syntax-case stx (define)
                (() '())
                (((define var val) rest rest* ...)
                 (begin
                   (def-list #'(define var #f))
                   #`((set! var val) #,@(xform #'(rest rest* ...)))))
                (((define var) rest rest* ...)
                 (begin
                   (def-list #'(define var #f))
                   (xform #'(rest rest* ...))))
                ((else rest ...)
                 #`(else #,@(xform #'(rest ...))))))))

      (append! (def-list) xformed))))

(define-syntax tlambda
  (lambda (stx)
    (syntax-case stx ()
      ((tlambda bind body body* ...)
       #`(lambda bind #,@(tlambda-scan-body #'(body body* ...)))))))

(define-syntax tlet
  (syntax-rules ()
    ((tlet ((var value) ...) form form* ...)
     ((tlambda (var ...) form form* ...) value ...))
    ((tlet loop ((var value) ...) form form* ...)
     (tlet ((loop #f))
       (set! loop (tlambda (var ...) form form* ...))
       (loop value ...)))))

(define-syntax tlet*
  (syntax-rules ()
    ((tlet* () form form* ...)
     (tlet () form form* ...))
    ((tlet* ((var value) varrest ...) form form* ...)
     (tlet ((var value)) (tlet* (varrest ...) form form* ...)))))
