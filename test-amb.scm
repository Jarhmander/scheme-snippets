(include "amb.scm")

(define (number-between lo hi)
  (let loop ((i lo))
    (if (> i hi)
        (amb)
        (amb i (loop (+ i 1))))))

(define solutions
  (bag-of-values
    (let ((i (number-between 0 200))
          (j (number-between 0 200)))
      (assert (< j i))
      (assert (even? (+ i j)))
      (assert (= (* j 3) (+ i j)))
      (values i j))))

(display solutions) (newline)
