(import (ice-9 control))

(define *iterator-types* (make-parameter '()))

(define (iterator-add-new-type pred ctor)
  (*iterator-types* (cons `(,pred . ,ctor) (*iterator-types*))))

(define (iterator-not-found-error)
  (error "iterator type not found"))

(define make-iterator
  (case-lambda
    ((obj)
     (make-iterator obj (lambda () (shift k k)))
    ((obj quit-k)
     (let search ((lst (*iterator-types*)))
        (if (null? lst)
            (iterator-not-found-error)
            (let* ((elem  (car lst))
                   (type? (car elem))
                   (ctor  (cdr elem)))
              (if (type? obj)
                  (ctor obj quit-k)
                  (search (cdr lst)))))))))
