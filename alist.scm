(define-syntax alist
  (syntax-rules ()
    [(alist k v)
     `((k . v))]
    [(alist k v rest ...)
     `((k . v) ,@(alist rest ...))]))
