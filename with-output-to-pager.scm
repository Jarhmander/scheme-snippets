;;; with-output-to-pager.scm
(use-modules (guile))
(use-modules (srfi srfi-1))

(define (with-output-to-pager thunk)
  (define-values (read-end write-end) (car+cdr (pipe)))
  (define pid (primitive-fork))
  (cond
    ((< pid 0) (error "fork failed!"))
    ((= pid 0)
     (dup2 (fileno read-end) 0)
     (for-each close (list read-end write-end))
     (execlp "less" "less" "-RFKLXe")
     (primitive-_exit))
    (else
      (let ((exception #f)
            (old-stdout (dup 1)))
        (dup2 (fileno write-end) 1)
        (for-each close (list read-end write-end))
        (let ((ret (with-continuation-barrier
                     (lambda ()
                       (catch #t
                         thunk
                         (lambda except-args
                           (set! exception except-args)))))))
          (close-fdes 1)
          (dup2 old-stdout 1)
          (close-fdes old-stdout)
          (waitpid pid)
          (if exception (apply throw exception))
          ret)))))
