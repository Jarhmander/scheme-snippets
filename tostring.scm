;;; ->string: convert all args to string and catenate

(define (->tostring . args)
  (with-output-to-string
    (lambda ()
      (for-each display args))))
