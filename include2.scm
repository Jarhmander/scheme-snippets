;;; implementation of "include"

(define-syntax inklude
  (lambda (stx)
    (define (read-file stx filename)
      (define f (open-input-file filename))
      (let read-loop ((form (read f)))
        (if (eof-object? form)
            (begin (close-port f) '())
            (cons (datum->syntax stx form)
                  (read-loop (read f))))))
    (syntax-case stx ()
      [(inklude filename)
       (with-syntax ([(forms ...) (read-file #'inklude (syntax->datum #'filename))])
         #'(begin forms ...))])))
