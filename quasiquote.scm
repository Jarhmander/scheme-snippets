;;;
;;; quasiquote, unquote & unquote-splicing are not primitives
;;;

(define-syntax kwasiquote
  (syntax-rules (unquote unquote-splicing)
    [(qq ((unquote e) e* ...))
     (cons e (kwasiquote (e* ...)))]
    [(qq ((unquote-splicing e) e* ...))
     (append e (kwasiquote (e* ...)))]
    [(qq ((e ...) e* ...))
     (cons (kwasiquote (e ...)) (kwasiquote (e* ...)))]
    [(qq (#(e ...) e* ...))
     (cons (kwasiquote #(e ...)) (kwasiquote (e* ...)))]
    [(qq (unquote e))
     e]
    [(qq (e e* ...))
     (cons (quote e) (kwasiquote (e* ...)))]
    [(qq #(e ...))
     (list->vector (kwasiquote (e ...)))]
    [(qq e)
     (quote e)]))
