(define-syntax kond
  (syntax-rules (else =>)
    [(kond (else f f* ...)) (f f* ...)]
    [(kond (pred f f* ...)) (if pred (begin f f* ...))]
    [(kond (pred => proc))  (let ((v pred))
                              if v (proc v))]
    [(kond (pred f f* ...) rest ...)
                            (if pred
                                (begin f f* ...)
                                (kond rest ...))]
    [(kond (pred => proc) rest ...)
                            (let ((v pred))
                              (if v
                                  (proc v)
                                  (kond rest ...))))
