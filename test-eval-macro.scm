
(define-syntax code
  (syntax-rules ()
    [(code args (var val) ... ret)
     (lambda args
       (let* ((var val) ...)
         ret))]))

(define f (eval (read)))

(format #t "v = ~A~%" (f 1))
