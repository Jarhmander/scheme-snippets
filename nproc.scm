(load-shared-object #f)

(define sched-getaffinity
  (foreign-procedure "sched_getaffinity"
    (int size_t u32*)
    int))

(define (nproc)
  (let* ((bv-len 1024)
         (bv     (make-bytevector bv-len))
         (r      (sched-getaffinity 0 bv-len bv))
         (int    (if (zero? r)
                     (bytevector-uint-ref bv 0 (endianness little) bv-len)
                     (error 'nproc "error getting affinity mask"))))
    (bitwise-bit-count int)))
