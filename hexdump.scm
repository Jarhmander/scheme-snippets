(import (srfi :1)
        (ice-9 format)
        (rnrs bytevectors))

(load "array-stride.scm")

(define (hexdump array)
  (format #t "~10t~{~@:(~2,'0X~)~^ ~}~%~10t~a~%" (iota 16) (make-string (+ 32 15) #\-))
  (for-each (lambda (i) 
              (let ((v (array->list (array-stride array i (+ i 16)))))
                (format #t "~@:(~8,'0X~): ~{~@:(~2,'0X~)~^ ~}~%" i v)))
            (iota (ceiling (/ (array-length array) 16)) 0 16)))
