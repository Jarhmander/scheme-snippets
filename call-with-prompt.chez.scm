;;; This is an emulation in Chez Scheme of call-with-prompt/abort-to-prompt found in guile

(define make-prompt-tag
  (case-lambda
    [()  (make-prompt-tag 'prompt)]
    [(p) (list p)]))

(define default-prompt-tag
  (make-parameter (make-prompt-tag)))

(define *prompt-alist*
  (make-parameter '()))

(define call-with-prompt
  (case-lambda
    [(thunk handler)
     (call-with-prompt (default-prompt-tag)
       thunk
       handler)]
    [(tag thunk handler)
     (call-with-current-continuation
       (lambda (return)
         (call-with-values
           (lambda ()
             (call-with-current-continuation
               (lambda (k)
                 (parameterize ([*prompt-alist* (cons (cons tag k) (*prompt-alist*))])
                   (call-with-values thunk return)))))
           handler)))]))

(define abort-to-prompt
  (lambda (tag . args)
    (let ([pp (assv tag (*prompt-alist*))])
      (unless pp
        (errorf 'abort-to-prompt "unknown prompt ~A" tag))
      (let ([k (cdr pp)])
        (call-with-current-continuation
          (lambda (this-k)
            (apply k this-k args)))))))

#|

(call-with-prompt
  (default-prompt-tag)
  (lambda ()
    (dynamic-wind
      (lambda () (format #t "enter...~%"))
      (lambda ()
        (format #t "before~%")
        (abort-to-prompt (default-prompt-tag) 42)
        (format #t "after~%"))
      (lambda () (format #t "leave...~%"))))
  (lambda args
    (format #t "handler got args: ~A~%" args)
    57))

does:

=| enter...
=| before
=| leave...
=| handler got args: (#<partial-continuation 7fa0cde88fa0> 42)
=> 57

|#
