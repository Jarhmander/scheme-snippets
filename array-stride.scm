;;;

(define (array-stride array start end)
  (let* ((array-end (array-length array))
         (end       (max 0 (min array-end (if (negative? end) (+ array-end end) end))))
         (len       (max 0 (- end start))))
    (make-shared-array array (lambda (i) (list (+ i start))) len)))
