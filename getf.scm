(define *not-found* '(not . found))

(define get-field
  (let* ((malformed-plist (lambda ()
                            (error "Malformed plist"))))
    (define-syntax check-car
      (syntax-rules ()
        [(check-car var)
         (if (pair? var)
             (car var)
             (malformed-plist))]))

    (lambda (lst member)
      (if (null? lst)
          *not-found*
          (let* ((k  (check-car lst))
                 (kr (cdr lst))
                 (v  (check-car kr))
                 (vr (cdr kr)))
            (if (eqv? k member)
                v
                (get-field vr member)))))))
