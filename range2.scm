;;;
;;; range, interchanged arguments for negative values
;;;

(import (srfi :71))

(define range
  (case-lambda
    [(u)    (iota u)]
    [(l u)  (range l u 1)]
    [(l u s)
     (let ((first last over? (if (positive? s)
                                 (values l u >=)
                                 (values u l <=))))
       (let loop ((i first))
         (if (over? i last)
             '()
             (cons i (loop (+ i s))))))]))
