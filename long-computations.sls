(library (long-computations (1 0 0))
  (export default-ticks
          long-computation
          let-long-computation
          progress-animator-char
          console-display-progress)
  (import (chezscheme))

(define default-ticks
  (make-parameter 500000
    (lambda (new)
      (unless (fixnum? new)
        (errorf 'default-ticks "~A is not a fixum" new))
      new)))

(define long-computation
  (case-lambda
    ((thunk progress-callback)
     (long-computation thunk progress-callback values))
    ((thunk progress-callback finish-callback/ticks)
     (if (procedure? finish-callback/ticks)
         (long-computation thunk progress-callback finish-callback/ticks (default-ticks))
         (long-computation thunk progress-callback values finish-callback/ticks)))
    ((thunk progress-callback finish-callback ticks)
     (letrec ((eng      (make-engine thunk))
              (run      (lambda (eng)
                          (eng ticks complete expired)))
              (complete (lambda (t . vals)
                          (apply finish-callback vals)))
              (expired  (lambda (new-eng)
                          (progress-callback (lambda () (run new-eng))))))
       (run eng)))))


(define-syntax let-long-computation
  (lambda (stx)
    (syntax-case stx (=>)
      ((wlc name ((var val) ...) => fn body body* ...) (for-all identifier? #'(name var ...))
       (with-syntax (((tmp ...) (generate-temporaries #'(var ...)))
                     ((vt  ...) (generate-temporaries #'(val ...))))
         #'(let ((f fn) (called-progress-at-least-once? #f) (vt val) ...)
             (let ((var vt) ...)
               (letrec ((name (lambda (tmp ...)
                                (critical-section
                                  (set! var tmp) ...)
                                (let ((var var) ...)
                                  body body* ...))))
                 (long-computation (lambda () (name vt ...))
                                   (lambda (cont)
                                     (set! called-progress-at-least-once? #t)
                                     (f #f var ...)
                                     (cont))
                                   (lambda vals
                                     (when called-progress-at-least-once?
                                       (f #t var ...))
                                     (apply values vals))))))))
      ((llc name ((var val) ...) exp body body* ...) (for-all identifier? #'(name var ...))
       #'(llc name ((var val) ...) => (lambda (done? var ...)
                                        exp)
           body body* ...)))))

(define (progress-animator-char str)
  (let ((i 0))
    (lambda ()
      (let ((ret (string-ref str i)))
        (set! i (modulo (+ i 1) (string-length str)))
        ret))))

(define console-display-progress
  (let ((char-animator (progress-animator-char "/-\\|")))
    (lambda (count total msg . args)
      (let* ((total-str (number->string total))
             (total-len (string-length total-str)))
        (format (current-output-port)
                "\r\x1B;[0J~C ~vD of ~A (~5,1,2F%): ~?"
                (char-animator) total-len count total-str (/ count total)
                msg
                args)
        (flush-output-port (current-output-port))))))

) ; library
