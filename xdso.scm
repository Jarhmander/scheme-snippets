(import (ice-9 iconv)
        (except (rnrs io ports) bytevector->string))

(load "hexdump.scm")

(define (load-binary-file filename)
  (call-with-input-file filename
    (lambda (p)
      (get-bytevector-all p))
    #:binary #t))

(define* (xdso b #:optional (start 0) (len #e4e9))
  (let* ((end (+ start len))
         (arr (array-stride b start end)))
    (hexdump arr)
    (bytevector->string (u8-list->bytevector (array->list arr)) "latin1")))
