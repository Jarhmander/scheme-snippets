(eval-when (compile)
    (include "read-raw-string.scm")
    (read-hash-extend #\"
                      (lambda (ch port) (read-raw-string port))))

(display #"=(Hello world!"aa\foo!)"oops!")=") ;" ;;;
