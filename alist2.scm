;;;
;;; Better alist
;;;

(define-syntax alist
  (syntax-rules ()
    [(alist)                '()]
    [(alist (k v) rest ...) `((k . ,v) ,@(alist rest ...))]))
