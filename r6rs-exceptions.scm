(library (my-rnrs exceptions)
  (export with-exception-handler
          raise
          raise-continuable
          guard)
  (import (except (rnrs)
            with-exception-handler
            raise
            raise-continuable
            guard)
          (only (guile) make-parameter parameterize)
          (ice-9 format))

(define (default-exception-handler e)
  (print-exception e)
  (unless (warning? e)
    ;; trig the debugger, or DIE!
    (exit -1)))

(define (get-base-condition-string e)
  (let ((who       (and (who-condition? e) (condition-who e)))
        (msg       (and (message-condition? e) (condition-message e)))
        (irritants (and (irritants-condition? e) (condition-irritants e))))
    (with-output-to-string
      (lambda ()
        (when (who-condition? e)
          (format #t "in ~a" (condition-who e)))
        (display #\:)
        (cond
          (msg
           (format #t " ~?" msg irritants))
          (irritants
           (format #t " with irritants ~S" irritants)))))))

(define (print-exception e)
  ;; TODO
  (format #t "got exception: ~S~%" (simple-conditions e))
  )

(define *exception-handlers* (make-parameter (list default-exception-handler)))

(define (with-exception-handler handler proc)
  (parameterize ((*exception-handlers* (cons handler (*exception-handlers*))))
    (proc)))

(define (raise-k e k)
  (let ((handler (car (*exception-handlers*)))
        (prev    (cdr (*exception-handlers*))))
    (parameterize ((*exception-handlers* prev))
      (call-with-values
        (lambda ()
          (handler e))
        k))))

(define (raise-continuable e)
  (raise-k e values))

(define (raise e)
  (let ((raise-non-continuable
          (lambda <ignored>
            (letrec* ((new-cond (make-non-continuable-violation))
                      (raise-loop (lambda <ignored>
                                    (raise-k new-cond raise-loop))))
              (raise-k new-cond raise-loop)))))
    (raise-k e raise-non-continuable)))

(define-syntax guard
  (syntax-rules (else)
    ((guard (var clauses ... (else else-actions ...)) body body* ...)
     (call-with-current-continuation
       (lambda (return)
         (with-exception-handler (lambda (var)
                                   (return (cond
                                             clauses ...
                                             (else else-actions ...))))
           (lambda ()
             body body* ...)))))
    ((guard (var (clause actions ...) ... ) body body* ...)
     (guard (var (clause actions ...) ... (else (raise-continuable e))) body body* ...))))

) ; library (my-rnrs exceptions)
