;;; Exceptions

(define-syntax try
  (syntax-rules (except)
    [(try f
       (except (key . args)
         hf hf* ...))
     (catch key
            (lambda () f)
            (lambda args hf hf* ...))]
    [(try f
       (except (k1 . a1) hf hf* ...)
       (except (kn . an) hn hn* ...)
       ...)
     (try
       (try f
         (except (k1 . a1) hf hf* ...))
       (except (kn . an) hn hn* ...)
       ...)]))
