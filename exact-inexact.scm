(define v1 (expt 100 100))      ; 100^100
(define v2 (expt 2 300))        ; 2^300
(define v3 (/ v1 v2))           ; v1 / v2, a rational number
(define v4 (exact->inexact v3)) ; force conversion to double

(format #t "v1 = ~s\nv2 = ~s\nv3 = ~s\nv4 = ~s\n" v1 v2 v3 v4)
