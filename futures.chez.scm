;;; Simple version of futures done in Chez Scheme
;;;
;;; For now, can create an unlimited amount of threads. It would be better to make a thread pool and
;;; grab a thread from the pool, or use the current thread to touch a future.

(import (chezscheme))

(define-record-type (futuredata %make-future future?)
  (fields
    (mutable value)
    (mutable ready)
    (mutable mutex)
    (mutable cond))
  (protocol
    (lambda (new)
      (lambda ()
        (new #f #f (make-mutex) (make-condition))))))

(define-syntax future
  (syntax-rules ()
    [(future exp)
     (let ([f (%make-future)])
       (fork-thread (lambda ()
                      (let ([c   (futuredata-cond f)]
                            [val exp])
                        (futuredata-value-set! f val)
                        (futuredata-ready-set! f #t)
                        (condition-signal c))))
       f)]))

(define (touch f)
  (define-syntax ready? (identifier-syntax (futuredata-ready f)))
  (unless ready?
    (let ([mut (futuredata-mutex f)]
          [c   (futuredata-cond f)])
      (let lp ()
        (with-mutex mut
          (condition-wait c mut))
        (unless ready? (lp)))))
  (futuredata-value f))
