(import (srfi :1)
        (srfi :43))

(define list-appender
  (case-lambda
    (()
     (list-appender #f '()))
    ((init-list)
     (list-appender #f init-list))
    ((copy? init-list)
     (let* ((init (if copy?
                      (list-copy init-list)
                      init-list))
            (self (cons '() init)))
       (set-car! self (if (null? init)
                          self
                          (last-pair init)))
       (case-lambda
         (()
          (cdr self))
         (args
          (set-cdr! (car self) args)
          (set-car! self (last-pair args))))))))

(define (list-appender* . args)
  (list-appender #f args))

(define list-prepender
  (case-lambda
    (()
     (list-prepender #f '()))
    ((init-list)
     (list-prepender #f init-list))
    ((copy? init-list)
     (let ((self (if copy?
                     (list-copy init-list)
                     init-list)))
       (case-lambda
         (()
          self)
         (args
          (set! self (append! args self))))))))

(define (list-prepender* . args)
  (list-prepender #f args))

(define-syntax define-inserter
  (syntax-rules ()
    ((define-inserter ctor-name (inserter result-builder))
     (define (ctor-name . init-values)
       (let ((self (apply inserter init-values)))
         (case-lambda
           (()
            (let ((ret (result-builder (self))))
              (set! self ret)
              self))
           (args
            (apply self args))))))))

(define-inserter vector-appender
  (list-appender* vector-concatenate))

(define-inserter vector-prepender
  (list-prepender* vector-concatenate))

(define-inserter string-appender
  (list-appender* string-concatenate))

(define-inserter string-prepender
  (list-prepender* string-concatenate))
