;;; for-each, regular and unpacking

(define-syntax for
  (syntax-rules (in do)
    [(for (v1 v* ...) in exp do
        e1 e* ...)
     (for elem in exp do
        (apply (lambda (v1 v* ...)
                 e1 e* ...)
               elem))]
    [(for var in exp do
        e1 e* ...)
     (for-each (lambda (var)
                 e1 e* ...)
               exp)]))
