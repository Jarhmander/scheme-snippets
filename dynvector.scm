(import (srfi :43)
        (rnrs records syntactic))

(define-record-type (dynvector %make-dynvector dynvector?)
  (fields
    (mutable vec)
    (mutable size)))

(define make-dynvector
  (case-lambda
    [(n) (make-dynvector n *unspecified*)]
    [(n e)
     (let ([vec (make-vector (max 1 n) e)])
       (%make-dynvector vec n))]))

(define-syntax define-dynvector-method
  (lambda (stx)
    (syntax-case stx ()
      [(ddm (name . args)
          body body* ...)
       (with-syntax ([vec (datum->syntax #'ddm 'vec)]
                     [size (datum->syntax #'ddm 'size)]
                     [self (datum->syntax #'ddm 'self)]
                     [new-name (datum->syntax #'ddm (symbol-append 'dynvector-
                                                                   (syntax->datum #'name)))])
         #'(begin
             (define (new-name self . args)
               (letrec-syntax
                 ([vec (identifier-syntax
                         [vec (dynvector-vec self)]
                         [(set! vec v) (dynvector-vec-set! self v)])]
                  [size (identifier-syntax
                          [size (dynvector-size self)]
                          [(set! size v) (dynvector-size-set! self v)])])
                 body body* ...))
             (define-syntax name
               (lambda (stx)
                 (with-ellipsis :::
                   (syntax-case stx ()
                     [(name targs :::)
                      (with-syntax ([self (datum->syntax #'name 'self)])
                        #'(new-name self targs :::))]))))))])))

(define (dynvector . elems)
  (let ((vec (list->vector elems)))
    (%make-dynvector vec
                     (vector-length vec))))

(define (dynvector->vector dvec)
  (let* ([sz (dynvector-size dvec)]
         [ret (make-vector sz)])
    (vector-copy! ret 0 (dynvector-vec dvec) 0 sz)
    ret))

(define (dynvector->list dvec)
  (vector->list (dynvector->vector dvec)))

(define (list->dynvector lst)
  (apply dynvector lst))

(define-dynvector-method (capacity)
  (vector-length vec))

(define dynvector-length dynvector-size)

(define-dynvector-method (check-ref who i upper-bound-check)
  (define (err msg)
    (error who msg i))

  (cond
    [(not (number? i))  (err "ref not a number: ~S")]
    [(inexact? i)       (err "ref not an exact number: ~S")]
    [(negative? i)      (err "ref is negative: ~S")]
    [(not (upper-bound-check i size))
     (err "ref out of bounds: ~S")]))

(define-dynvector-method (ref i)
  (check-ref 'dynvector-ref i <)
  (vector-ref vec i))

(define-dynvector-method (front)
  (ref 0))

(define-dynvector-method (back)
  (ref (- size 1)))

(define (dynvector-set! dvec i v)
  (dynvector-check-ref dvec 'dynvector-set! i <)
  (vector-set! (dynvector-vec dvec) i v))

(define-dynvector-method (reserve! sz)
  (define cap (capacity))
  (when (> sz cap)
    (let* ([new-sz (do ([s cap (capacity-growth s)])
                       [(>= s sz) s])]
           [new-vec (make-vector new-sz)])
      (vector-copy! new-vec 0 vec)
      (set! vec new-vec))))

(define (capacity-growth i)
  (if (zero? i)
      1
      (* 2 i)))

(define-dynvector-method (reserve-more! incr)
  (reserve! (+ size incr)))

(define-dynvector-method (reserve-at! pos len)
  (check-ref 'dynvector-reserve-at! pos <=)
  (reserve-more! len)
  (vector-copy! vec (+ pos len) vec pos (- (capacity) len))
  (set! size (+ size len)))

(define-dynvector-method (insert! pos . elems)
  (check-ref 'dynvector-insert! pos <=)
  (reserve-at! pos (length elems))
  (do ([pos   pos   (+ pos 1)]
       [elems elems (cdr elems)])
      [(null? elems) self]
    (dynvector-set! self pos (car elems))))

(define-dynvector-method (push-back! . elems)
  (let ([pos size])
    (apply dynvector-insert! self pos elems)))

(define dynvector-remove!
  (case-lambda
    [(self from)
     (dynvector-remove! self from (1+ from))]

    [(self from to)
     (check-ref 'dynvector-remove from <)
     (check-ref 'dynvector-remove to <=)
     (when (> from to)
       (error 'dynvector-remove "invalid range: (~S ~S)" from to))

     (let ([sz (dynvector-size self)]
           [vec (dynvector-vec self)])
       (vector-copy! vec from vec to)
       (dynvector-size-set! self (- sz (- to from))))]))

(define-dynvector-method (pop!)
  (let* ([pos (- size 1)]
         [ret (ref pos)])
    (dynvector-remove! self pos)
    ret))
