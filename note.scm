(import (only (rnrs base)
              let*-values log)
        (rnrs records syntactic))

(define-record-type note
  (fields
    (mutable octave)
    (mutable scale)
    (mutable cents)))

(define (frequency->midi-note f)
  (+ (* 12 (log (/ f 440) 2)) 57))

(define (midi-note->frequency m)
  (* 440 (expt 2 (/ (- m 57) 12))))

(define (midi-note->note m)
  (let*-values (((note-round)   (round m))
                ((octave scale) (floor/ note-round 12))
                ((cents)        (* 100 (- m note-round))))
    (make-note octave scale cents)))

(define (note->midi-note n)
  (let ((octave (note-octave n))
        (scale  (note-scale n))
        (cents  (note-cents n)))
    (+ (* 12 octave) scale (/ cents 100))))

(define (frequency->note f)
  (midi-note->note (frequency->midi-note f)))

(define (note->frequency n)
  (midi-note->frequency (note->midi-note n)))
