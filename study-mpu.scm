(import (rnrs))

(define-record-type mpu-bits
  (fields
    addr
    size
    access?))

(define get-values
  (let-syntax ([err-port (identifier-syntax (current-error-port))])
    (lambda ()
      (define query
        (lambda (name)
          (format err-port "Enter ~A: ~16,4T" name)
          (let ([r (read)])
            (cond
              [(eof-object? r)
               (newline)
               (exit)]
              [(integer? r) r]
              [else
               (format err-port "error: supply an integer (value = ~A)~%" r)
               (query name)]))))

      (let* ([a (query "start")]
             [b (query "end")])
        (values a b)))))

(define-syntax loop
  (syntax-rules ()
    [(loop b ...) (let lp ()
                    b ...
                    (lp))]))

(define show-value
  (lambda (msg v)
    (format #t "~A = ~16,4T0x~4,'0X ~:*~5D ~:*(0b~16,'0B)~%" msg (logand #xFFFF v))))

(define-syntax show
  (lambda (stx)
    (syntax-case stx ()
      [(show var) (identifier? #'var)
       #'(show 'var var)]
      [(show var val) #'(show-value var val)])))

(define (void) (if #f #f))

(define-syntax let*-show
  (lambda (stx)
    (define (expand-var stx)
      (map (lambda (stx)
             (syntax-case stx (: =)
               [(var : val) (identifier? #'var)
                #'[(var) (let ([t val])
                           (show 'var t)
                           t)]]
               [(var ... : expr) (for-all identifier? #'(var ...))
                (with-syntax ([(t ...) (generate-temporaries #'(var ...))])
                  #'[(var ...) (let-values ([(t ...) expr])
                                 (show 'var t) ...
                                 (values t ...))])]
               [(var = val) (identifier? #'var)
                #'((var) val)]
               [(var ... = expr) (for-all identifier? #'(var ...))
                #'[(var ...) expr]]
               [((var ...) val) (for-all identifier? #'(var ...))
                #'[(var ...) val]]
               [(var val) (identifier? #'var)
                #'((var) val)]
               [var (identifier? #'var)
                #'((var) (void))]))
           stx))

    (syntax-case stx ()
      [(lss (v ...) b ...)
       #`(let*-values #,(expand-var #'(v ...))
           (void) b ...)])))

(define (bit-diff start end)
  (logxor start (- end 1)))

(define (bit-diff-length start end)
  (bitwise-length (bit-diff start end)))

(define (make-mask n)
  (- (ash 1 n) 1))

(define (need-split? start end)
  (let ([diff (- end start)]
        [region-size (make-mask (bit-diff-length start end))])
    (> region-size (* 4 diff))))

(define (split-region start end)
  (let* ([split-region-mask (make-mask (- (bit-diff-length start end) 1))]
         [midpoint          (+ (logior start split-region-mask) 1)])
    (values (list start midpoint)
            (list midpoint end))))

(define (main)
  (loop
    (let*-show ([start end   : (get-values)]
                [end-1       : (- end 1)]
                [diff        : (- end start)]
                [bitorder    : (bit-diff-length start end)]
                [region-size : (ash 1 bitorder)]
                [mask        : (make-mask bitorder)])
               (cond [(need-split? start end)
                      (display "must split the region\n")
                      (let*-show ([end-i-1    : (logior start (ash mask -1))]
                                  [start-i    : (+ 1 end-i-1)]
                                  [region-1   : (bit-diff-length start start-i)]
                                  [region-2   : (bit-diff-length start-i end)]
                                  ))]
                     [else (display "no need to split the region\n")]))))

(unless (top-level-bound? 'test)
  (main))
