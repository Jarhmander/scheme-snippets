(eval-when (expand eval compile load)
  (load "with-implicit.scm")
  (load "with-slots.scm"))

(import (rename (oop goops)
          (define-class %define-class)))

(define-syntax define-class
  (lambda (stx)
    (define (collect-slots stxlist)
      (syntax-case stxlist ()
        [() '()]

        [((slot options ...) rest ...)
         (cons (datum slot)
               (collect-slots #'(rest ...)))]

        [(sym/keyword rest ...)
         (let ((e (datum sym/keyword)))
           (cond [(keyword? e)
                  ;; terminate, the remaining elements are keyword args
                  '()]

                 [(symbol? e)
                  (cons e (collect-slots #'(rest ...)))]

                 [else
                  ;; completely unexpected?
                  (collect-slots #'(rest ...))]))]))

    (syntax-case stx ()
      [(define-class classname (parents ...)
         slot/option ...)
       (let ([name  (datum classname)])
         (with-implicit (define-class
                          [new-classname (symbol-append '< name '>)]
                          [define-method-name (symbol-append 'define- name '-method)]
                          [stx-slots (collect-slots #'(slot/option ...))])
           #'(begin
               (%define-class new-classname (parents ...)
                              slot/option ...)

               (define-syntax define-method-name
                 (lambda (stx)
                   (with-ellipsis …
                     (syntax-case stx ()
                       [(dmn (name self . args)
                             b b* …)
                        (letrec-syntax ((bind-slots
                                          (syntax-rules ()
                                            [(ev () r)
                                             r]
                                            [(ev (e e* …) r)
                                             (with-implicit (dmn (var (symbol-append (datum self) ': 'e)))
                                               #`(with-slots ((var e)) self
                                                   #,(bind-slots (e* …) r)))])))
                          #`(define-method (name (self new-classname) . args)
                              #,(bind-slots stx-slots #'(begin b b* …))))])))))))])))
