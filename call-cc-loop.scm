;;; loop using call/cc

(let ()
  (define i 0)
  (define f
    (let ((c (call/cc
               (lambda (c)
                 c))))
      (lambda ()
        (c c))))

  (unless (> i 5)
    (format #t "i = ~a\n" i)
    (set! i (+ i 1))
    (f)))
