;;; for-each, regular, unpacking, with break and continue

(define-syntax for
  (lambda (stx)
      (syntax-case stx (in do)
        [(for (v1 v* ...) in exp do
           e1 e* ...)
         #'(for elem in exp do
              (apply (lambda (v1 v* ...)
                       e1 e* ...)
                     elem))]
        [(for var in exp do
           e1 e* ...)
         (with-syntax ((break (datum->syntax #'for 'break))
                       (continue (datum->syntax #'for 'continue)))
           #'(call/cc (lambda (break)
                        (for-each (lambda (var)
                                    (call/cc (lambda (continue)
                                               e1 e* ...)))
                                    exp))))])))
