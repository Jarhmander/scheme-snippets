;;;
;;; range, python style
;;;

(define range
  (case-lambda
    [(u)    (iota u)]
    [(l u)  (range l u 1)]
    [(l u s)
     (let ((over? (if (positive? s) >= <=)))
       (let loop ((i l))
         (if (over? i u)
             '()
             (cons i (loop (+ i s))))))]))
