(define *amb-fail* (make-parameter
                     (lambda ()
                       (error "amb tree exhausted!"))))

(define (amb-fail)
  ((*amb-fail*)))

(define (amb-lazy-list p-list)
  (call/cc
    (lambda (return)
      (let ((old-fail (*amb-fail*)))
        (for-each (lambda (choice)
                    (call/cc
                      (lambda (backtrack-point)
                        (*amb-fail* backtrack-point)
                        (call-with-values choice return))))
                  p-list)
        (*amb-fail* old-fail)
        (amb-fail)))))


(define-syntax amb
  (syntax-rules ()
    [(amb)              (amb-fail)]
    [(amb choice)       (choice)]
    [(amb choices ...)  (amb-lazy-list (list (lambda () choices) ...))]))

           ;; All failed

(define (assert pred)
  (if (not pred) (amb)))

(define-syntax bag-of
  (syntax-rules ()
    [(bag-of ex)
     (let ((old-fail (*amb-fail*))
           (result '()))

       (call/cc (lambda (c)
                  ;; When the outer (amb) fails, quit this continuation.
                  (*amb-fail* c)

                  ;; Otherwise, each time we force "failure", we return at this (let), extracting
                  ;; another solution.
                  (let ((v ex))
                    (set! result (cons v result))

                    (amb-fail))))
       (*amb-fail* old-fail)
       (reverse! result))]))

(define-syntax bag-of-values
  (syntax-rules ()
    [(bag-of-values ex)
     (bag-of (call-with-values (lambda () ex) list))]))
