;;; Read raw strings

(define read-raw-string
  (lambda (port)
    (let-syntax ((if-next-char
                   (syntax-rules ()
                     [(_ (var comp) then else)
                      (let ((var (read-char port)))
                        (if (char=? var comp) then else))])))
      (letrec ((str-open
                 (lambda (str-delim)
                   (if-next-char (ch #\()
                                 (str-loop str-delim "")
                                 (str-open (string-append str-delim
                                                          (string ch))))))
               (str-loop
                 (lambda (str-delim str-contents)
                   (if-next-char (ch #\))
                                 (str-closing str-delim str-contents 0)
                                 (str-loop str-delim
                                           (string-append str-contents
                                                          (string ch))))))
               (str-closing
                 (lambda (str-delim str-contents i)
                   (if (= i (string-length str-delim))
                       (str-close str-delim str-contents)
                       (if-next-char (ch (string-ref str-delim i))
                                     (str-closing str-delim str-contents (+ i 1))
                                     (str-loop str-delim (string-append str-contents
                                                                        ")"
                                                                        (substring str-delim 0 i)
                                                                        (string ch)))))))
               (str-close
                 (lambda (str-delim str-contents)
                   (if-next-char (ch #\")
                                 str-contents
                                 (str-loop str-delim
                                           (string-append str-contents
                                                          ")"
                                                          str-delim
                                                          (string ch)))))))
        (str-open "")))))
