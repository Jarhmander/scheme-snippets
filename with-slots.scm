;;;
;;; with-slots
;;;

(import (oop goops))

(define-syntax with-slots
  (syntax-rules ()
    ((with-slots (bindings ...) obj
       form form* ...)
     (with-slots "bind" ((obj bindings) ...)
       form form* ...))

    ((with-slots "bind" ()
       form form* ...)
     (begin form form* ...))

    ((with-slots "bind" ((obj (var slot)) rest ...)
       forms ...)
     (let-syntax
       ((var (identifier-syntax (var          (slot-ref obj 'slot))
                                ((set! var v) (slot-set! obj 'slot v)))))
       (with-slots "bind" (rest ...)
         forms ...)))

    ((with-slots "bind" ((obj var) rest ...)
       forms ...)
     (with-slots "bind" ((obj (var var)) rest ...)
       forms ...))))
